angular.module('exponeaTest')
.directive('extPanel', function () {
    return {
        transclude: {
            'title': '?extPanelTitle',
            'body': '?extPanelBody',
            'footer': '?extPanelFooter'
        },
        replace:true,
        scope:{},
        templateUrl: 'layout/directives/ext-panel.html',
        link: function(scope, element, attr, ctrl, transclude) {
            scope.showBody = transclude.isSlotFilled('body');
            scope.showFooter = transclude.isSlotFilled('footer');
        }
    };
});