angular.module('exponeaTest',['ngRoute',
                              'ui.bootstrap',
                              'ui.bootstrap.ext',
                              'oc.lazyLoad',
                              'neDirectives',
                              'neRest',
                              'neGrid',
                              'neLocal',
                              'neModals',
                              'neNotifications',
                              'neObject',
])
.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/dashboard', { templateUrl:'dashboard/views/index.html', controller:'DashboardCtrl', reloadOnSearch:false })
        .otherwise({redirectTo: '/dashboard'});

    // lazy load route
    //.when('/cms/documents',{
    //    templateUrl:'/dms/yourmodule/template.html',
    //    controller:'yourControllerCtrl',
    //    resolve:{
    //        loadModule:['$ocLazyLoad',function($ocLazyLoad){
    //            return $ocLazyLoad.load({
    //                name:'dms.yourmodule',
    //                files:['/dms/yourmodule/yourmodule.js']
    //            });
    //        }]
    //    }
    //})
}])
.factory('exponeaTest', ['NeRestResource','neNotifications', function(Resource, notify){
    var exponeaTest = this;
    
    var defaultRestOpts = {

        // parsing
        dataKey:'data', // data key, if data is property of response object, e.g. { data:..., status:...}
        resourceListKey: 'this', // list of resources - if there is no wrapper in response object, data is resource, resourceListKey:'this'
        resourceKey: 'this', // single resource data - if there is no wrapper in response object, data is resource, resourceKey:'this'
        idKey:'id', // key of id, sometimes id is represented by another key, like "_id", or "productId"
        errorKey:'data', // if response status !== 200, parse errors

        // if response contains pagination
        paginationCountKey:'pagination.count',
        paginationPageKey:'pagination.page',
        paginationPagesCountKey:'pagination.pages',
        paginationHasNextKey:'pagination.next',
        paginationHasPrevKey:'pagination.prev',

        responseErrors: {
//            '400': showErrorMessage,
//            '401':function(data, status, headers){
//                dms.loginModal.show();
//            },
//            '403':showErrorMessage,
//            '404':function(data, status, headers){
//                notify.error('Document not found','Try refresh page, please');
//            },
//            '409':showErrorMessage,
//            'default':showErrorMessage
        },

        commands:{
            create:{
                onSuccess: function(status, data){
                    notify.success('Created', 'successfully');
                }
            },
            update:{
                onSuccess: function(status, data){
                    notify.success('Updated', 'successfully');
                }
            },
            remove:{
                onSuccess: function(status, data){
                    notify.success('Removed', 'successfully');
                }
            }
        }
    };

    
    exponeaTest.features = new Resource(defaultRestOpts, {
        baseUrl:'features',
        commands:{
            getFunnels:{
                method:'GET',
                url:'/features/funnels'
            }
        }
    });
    
    return exponeaTest;
}]);