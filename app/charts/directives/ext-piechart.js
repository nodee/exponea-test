/*
 * PIE CHART
 * 
 * @example:
 * data as name:value
 * <ext-piechart chart-data="{'Kosice':50,'Bratislava':25,'Zvolen':25}"></ext-piechart>
 * 
 * data as array with mapping
 * <ext-piechart chart-data="[{someLabel:'Kosice',someValue:50},{someLabel:'Bratislava',someValue:25},{someLabel:'Zvolen',someValue:25}]" 
                 chart-data-name="'someLabel'"
                 chart-data-value="'someValue'">
   </ext-piechart>
 * 
 */

angular.module('exponeaTest')
.directive('extPiechart', ['neObject','extChartColors', function(object, extChartColors) {
    return {
        restrict: 'E',
        templateUrl: 'charts/directives/ext-piechart.html',
        scope:{
            data:'=chartData',
            nameMapping:'=chartDataName',
            valueMapping:'=chartDataValue',
        },
        link: function(scope, element, attr) {
            scope.$watch('data', refreshChart, true);
            
            function refreshChart(pieData){
                scope.chartItems = [];
                if(!pieData) return;
                
                var i = 0, 
                    key,
                    dataItem,
                    angle = 0,
                    total = 0,
                    startAngle = 0,
                    endAngle = 0,
                    x1 = 0,
                    x2 = 0,
                    y1 = 0,
                    y2 = 0;
                
                // calculate total, reset chartItems
                if(object.isObject(pieData)){
                    for(key in pieData){
                        if(pieData.hasOwnProperty(key)){
                            dataItem = getDataItem(pieData, key);
                            scope.chartItems.push(dataItem);
                            total += dataItem.value;
                        }
                    }
                }
                else if(object.isArray(pieData)){
                    for(i=0;i<pieData.length;i++){
                        dataItem = getDataItem(pieData, i);
                        scope.chartItems.push(dataItem);
                        total += dataItem.value;
                    }
                }
                else throw new Error('extPieChart: chart data must be object or array, and is "' +(typeof pieData)+ '"');
                
                function getDataItem(pieData, key){
                    return {
                        name: scope.nameMapping ? object.deepGet(pieData[key], scope.nameMapping) : key,
                        value: scope.valueMapping ? object.deepGet(pieData[key], scope.valueMapping) : pieData[key]
                    };
                }

                // calculate pie chart angels
                for(i=0; i < scope.chartItems.length; i++){
                    angle = Math.ceil(360 * scope.chartItems[i].value/total);
                    startAngle = endAngle;
                    endAngle = startAngle + angle;

                    x1 = parseInt(200 + 180*Math.cos(Math.PI*startAngle/180));
                    y1 = parseInt(200 + 180*Math.sin(Math.PI*startAngle/180));

                    x2 = parseInt(200 + 180*Math.cos(Math.PI*endAngle/180));
                    y2 = parseInt(200 + 180*Math.sin(Math.PI*endAngle/180));                

                    scope.chartItems[i].d = 'M200,200  L' + x1 + ',' + y1 + '  A180,180 0 0,1 ' + x2 + ',' + y2 + ' z';
                    scope.chartItems[i].color = extChartColors.index(i);
                }
            }
            
            
        }
    };
}]);