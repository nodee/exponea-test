angular.module('exponeaTest')
.service('extChartColors',[function(){
    
    var colors = ['#468966','#FFF0A5','#FFB03B','#B64926','#8E2800'];
    
    return {
        all: function(){
            return colors;
        },
        index: function(i){
            return colors[i];
        },
        random: function(){
            throw new Error('Not implemented yet');
        }
    };
}]);