angular.module('exponeaTest')
.controller('DashboardCtrl',['$scope', 'exponeaTest', 'NeGrid', function($scope, exponeaTest, Grid){
    
    $scope.grid = new Grid({
        id: 'dashboard.featuresGrid',
        resource: exponeaTest.features,
        multiSelect: false,
        defaultSort:{},
        defaultQuery:{},
        limit:10,
        autoLoad: true
    }).load();
    
}]);