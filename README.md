# Exponea Test

## Requirements
```
node.js >= 4.0.0
```

## Installation
```
git pull https://bitbucket.org/nodee/exponea-test.git
cd exponea-test
npm install
```

## Run
```
node debug.js
```