
exports.install = function() {
    framework.route('/usage', usage, ['get']);
};

function usage(){
    this.json(framework.usage(true), {}, true);
}