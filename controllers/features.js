
exports.install = function() {
    framework.route('/features', sendFakeData, ['get']);
};

function sendFakeData(){
    this.json(generateFakeData());
}

function generateFakeData(){
    return {
        "data": [{
            "type":"funnel",
            "firstAppVisit":12,
            "something":123
        }, {
            "type":"report",
            "firstAppVisit":22,
            "something":223
        }, {
            "type":"retention",
            "firstAppVisit":32,
            "something":323
        }, {
            "type":"segmentation",
            "firstAppVisit":42,
            "something":423
        }],
        "pagination": {
            "page": 1,
            "pages": 1,
            "limit": 10,
            "next": false,
            "prev": false,
            "count": 4
        }
    };
}